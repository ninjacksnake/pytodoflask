# IMPORTANDO DEPENDENCIAS
from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql.schema import PrimaryKeyConstraint

# CREANDO NUESTRA APLICACION FLASK
app = Flask(__name__)

# DEFINIENDO NUESTRAS CONFIGURACIONES PARA LA LIBRERIA DE BASE DE DATOS SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# CREANDO LA BASE DE DATOS PARA LA APP
db = SQLAlchemy(app)


# CREANDO LAS TABLAS PARA LA BASE DE DATOS
class Tarea(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    titulo = db.Column(db.String(200))
    completada = db.Column(db.Boolean)


class Usuario(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(50))
    password = db.Column(db.String(8))


# DEFINIENDO LAS RUTAS
@app.route('/<int:logueado>')
def index(logueado):
    print(logueado)
    # MOSTRANDO LAS TAREAS
    lista_tareas = Tarea.query.all()
    if(logueado == 1):
        return render_template('index.html', lista_tareas=lista_tareas)
    else:
        return redirect(url_for('login'))

#RUTA PARA EL LOGIN 
@app.route('/login')
def login():
    return render_template('login.html')

#POST PARA LOGUEARSE
@app.route('/login', methods=['POST'])
def logearse():
    username = request.form.get('username')
    password = request.form.get('password')
    usuario = Usuario.query.filter_by(nombre=username).first()
    if(usuario.nombre == username and usuario.password == password):
            return redirect(url_for('index',logueado = 1))
    else:
            return redirect(url_for('login'))
      # RECARGAR LA PAGINA

#POST PARA AGREGAR TAREA
@app.route('/add', methods=['POST'])
def add_tarea():
    titulo = request.form.get('titulo')
    nuevaTarea = Tarea(titulo=titulo, completada=False)
    db.session.add(nuevaTarea)
    db.session.commit()
    return redirect(url_for('index',logueado = 1))  # RECARGAR LA PAGINA

    # BORRAR TAREA

#DELETE PARA BORRAR TAREA
@app.route('/delete/<int:todo_id>')
def borrar_tarea(todo_id):
    logueado = True
    tarea = Tarea.query.filter_by(id=todo_id).first()
    db.session.delete(tarea)
    db.session.commit()
    return redirect(url_for('index',logueado = 1))  # RECARGAR LA PAGINA

    # COMPLETAR / DESCOMPLETAR TAREA

#UPDATE PARA ACTUALIZAR COMPLETAR/DESCOMPLETAR TAREA
@app.route('/update/<int:todo_id>')
def actualizar_tarea(todo_id):
    # ACTUALIZAR TAREAS
    logueado = True
    tarea = Tarea.query.filter_by(id=todo_id).first()
    tarea.completada = not tarea.completada
    db.session.commit()
    return redirect(url_for('index',logueado = 1))  # RECARGAR LA PAGINA


# SETEANDO LAS CONFIGURACIONES CON LAS QUE CORRERA LA app
if __name__ == '__main__':
    db.create_all()
    # CREANDO DATOS DE PRUEBA  ->
    # CREANDO EL USUARIO ADMIN
    # admin_user = Usuario(nombre="Admin", password="1234")
    # CREANDO UNA TAREA
    # nuevaTarea = Tarea(titulo="Nueva Tarea de Prueba", completada=False)
    # db.session.add(admin_user) #AGREGANDO USUARIO
    # db.session.add(nuevaTarea) #AGREGANDO LA TAREA
    # db.session.commit() #GUARDANDO EN LA DB

    app.run(debug=True)  # iniciar en modo debugger
